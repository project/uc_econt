Module provides a method of delivery "Econt Delivery" and method of payment
"Econt cash" for Ubercart2. When enabled and configured modules, upon
completion of the contract for completing the delivery address, the customer
has a choice of preferred options for delivery. Based on selected products,
shipping address and preferred delivery options, calculate the cost of delivery
of the order. In selected payment method "Econt COD" is automatically charged
the fee for this.
The calculation of the cost of delivery and cash on delivery fee is calculated
by the online system Econt. Then apply to your order at the online store.

For these contracts the site administrator can click one to generate a bill,
which is recorded in his account in the system Econt.

To use the module, the store owner must have an online account system Econt
http://econt.com. Getting an account is quick and without formalities.

Installation, setup and use of the module is online documentation of
http://d-support.eu/node/53.

Questions on the module you can set the forum site http://d-support.eu/forum,
through the contact form, email econt at d-support.eu, and Issue to the module.

UPDATE 1

Download and import some necessary data:
Download data is not fully automated - you must run download procces for each of
cities, quarters, streets and offices tables manualy -
admin/store/settings/quotes/methods/econt/econt_data_download
Then visit and imort downloaded data
admin/store/settings/quotes/methods/econt/econt_data_import

IMPORTANT FOR IMPORT streets
PHP
memory_limit = 196M